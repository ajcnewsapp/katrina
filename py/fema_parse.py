import pandas
 
# Load the data into a DataFrame
data = pandas.read_csv('fema_metro_us.csv')

bymetro = data.groupby('CBSA_CODE')

print bymetro['RelativeFitness'].describe()

df['a_bsum'] = df.groupby('CBSA_CODE')['Count'].transform(sum)
df.sort(['a_bsum','METRO_DIVISION_LSAD'], ascending=[True, False]).drop('a_bsum', axis=1)