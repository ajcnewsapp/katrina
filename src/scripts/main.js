$(window).on('resize',function(){

  window.h = $(window).height()-200;
  window.w = $(window).width();

  window.mapw = $('#map').width()*1.25;
  window.maph = $('#map').width();

}).resize();


var currentID = 0,
  zoomSouth = false, metroCircles = false, zoomGA = false;

var tts = _.template('<p class="region"><%= region %></p><p class="count"><%= count %></p>');

var us = topojson.feature(us, us.objects.us_fema).features,
    metros = topojson.feature(metros, metros.objects.metros).features,
    georgia = topojson.feature(georgia, georgia.objects.georgia_zips).features,
    atl = topojson.feature(atl, atl.objects.metro_counties).features,
    atl_zips = topojson.feature(atl_zips, atl_zips.objects.atl_zips).features;

var projection = d3.geo.albersUsa()
  .scale(mapw-100)
  .translate([mapw/2.5, h/2.5]);

var path = d3.geo.path()
    .projection(projection);

var svg = d3.select("#map").append("svg")
    .attr("width", w-100)
    .attr("height", h);


var quantize = d3.scale.quantize()
  .domain([0, 15000])
  .range(['#b0d6e9','#98bcce','#80a3b4','#688a9a','#527282','#3c5b69','#274552','#12303c'])


var states = svg.append("g")
    .attr("class", "us")
  .selectAll("path")
    .data(us)
  .enter().append("path")
    .attr("id", function(d) { return d.properties.STUSPS })
    .attr("d", path)
    .style("fill", function(d) { 
      return quantize(+d.properties.COUNT)
    });


MapFunctions = {

  drawStates: function(){

    if (zoomSouth) {
      svg.selectAll("circle").remove();
      states.transition()
        .duration(750)
        .attr("transform", "scale(" + 1 + ")");
      zoomSouth = false;
    }

    zoomSouth = false;
    

    states.on('mousemove', function(d){
        var xPos = event.clientX + document.body.scrollLeft;
        var yPos = event.clientY + document.body.scrollTop; 
        $("#tooltip")
          .show()
          .css("left", xPos - 30 + 'px')
          .css("top", yPos + 20 + 'px')
          .html(tts({region:d.properties.NAME,count:d.properties.COUNT}))
    }).on('mouseout', function(d){ 
      $('#tooltip').hide()
    });

  },

  drawMetroDots: function(){

    zoomSouth = true;

    if (metroCircles){
      svg.selectAll("circle")
        .transition().delay(function(d,i) { return (i * 40); }).each(function(){
          d3.select(this)
            .transition()
            .attr("r","2")
            .style("opacity","1")
        });

      metroCircles = false;
    }

    var d = _.filter(us,function(m){ return m.properties.STUSPS==='MS' })[0]
    var x, y, k;

    var centroid = path.centroid(d);
    x = centroid[0];
    y = centroid[1];
    k = 2.5;

    states.transition()
        .duration(750)
        .attr("transform", "translate(" + mapw / 2.5 + "," + h / 2.5 + ")scale(" + k + ")translate(" + -x + "," + -y + ")");


    states.on('mousemove', null)
      .style("cursor","inherit");


    var circles = svg.selectAll("circle")
      .data(metros)
      .sort(function(a, b) { return b.properties.fema_metros_count - a.properties.fema_metros_count })
      .enter()
      .append("circle")
      .attr("transform", "translate(" + mapw / 2.5 + "," + h / 2.5 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
      .attr("class","metro-dot")
      .attr("cx", function(d){return projection(d.geometry.coordinates)[0];})
      .attr("cy", function(d){return projection(d.geometry.coordinates)[1];})
      .attr("r", "0")
      .style("opacity","0")
      .transition().delay(function(d,i) { return (i * 40)+800; }).each(function(){
        d3.select(this)
          .transition()
          .attr("r","2")
          .style("opacity","1")
      })

  },

  drawMetroCircles: function(){
    

    if (zoomGA) {
      svg.selectAll(".georgia").transition().remove();
      var d = _.filter(us,function(m){ return m.properties.STUSPS==='MS' })[0]
      var x, y, k;

      var centroid = path.centroid(d);
      x = centroid[0];
      y = centroid[1];
      k = 2.5;

      states.transition().delay(700)
        .duration(750)
        .attr("transform", "translate(" + mapw / 2.5 + "," + h / 2.5 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
        .style("opacity", "1")
    }

    zoomGA = false;
    metroCircles = true;
    var radius = d3.scale.sqrt()
      .domain([0, 1e6])
      .range([0, 30]);

    svg.selectAll(".metro-dot")
      .transition()
      .delay(function(d,i) { return (i * 40); })
      .each(function(){
        d3.select(this)
          .transition()
          .style("opacity","1")
          .attr("r", function(d) { 
            return radius(d.properties.fema_metros_count)
          });
      });
  },

  georgiaZips: function(){
    zoomGA = true;
    svg.selectAll(".metro-dot")
      .transition()
      .delay(function(d,i) { return (i * 40); })
      .each(function(){
        d3.select(this)
          .transition()
          .attr("r", "0")
          .style("opacity","0");
      });


    var d = _.filter(us,function(m){ return m.properties.STUSPS==='GA' })[0]
    var x, y, k;

    var centroid = path.centroid(d);
    x = centroid[0];
    y = centroid[1];
    k = 6;

    states.transition()
        .delay(750)
        .duration(750)
        .attr("transform", "translate(" + mapw / 3 + "," + h / 2.2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
        .style("opacity", "0.3").style("stroke","none");


    var this_quantize = quantize;
    this_quantize.domain([d3.min(georgia, function(g){ return +g.properties.count }),d3.max(georgia, function(g){ return +g.properties.count })])

    var zips = svg.append("g")
      .attr("class", "georgia")
      .selectAll("path")
        .data(georgia)
      .enter().append("path")
      .style("opacity","0")
      .attr("transform", "translate(" + mapw / 3 + "," + h / 2.2 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
      .attr("d", path);
      
      zips.transition().delay(1100)
          .style("fill", function(d) { 
            return this_quantize(+d.properties.count);
          })
          .style("opacity","1");


      zips.on('mousemove', function(d){
        var xPos = event.clientX + document.body.scrollLeft;
        var yPos = event.clientY + document.body.scrollTop; 
        $("#tooltip")
          .show()
          .css("left", xPos - 30 + 'px')
          .css("top", yPos + 20 + 'px')
          .html(tts({region:'Zip: '+d.properties.ZCTA,count:d.properties.count}))
      }).on('mouseout', function(d){ 
        $('#tooltip').hide()
      });


  },
  metroATL: function() {
    zoomATL = true;

    var d = _.filter(atl,function(m){ return m.properties.name==='Fulton' })[0]
    var x, y, k;

    var centroid = path.centroid(d);
    x = centroid[0];
    y = centroid[1];
    k = 20;

    var this_quantize = quantize;
    this_quantize.domain([d3.min(atl_zips, function(g){ return +g.properties.fema_count }),d3.max(atl_zips, function(g){ return +g.properties.fema_count })]);


    states.on('mousemove', null)
      .style("cursor","inherit");

    svg.selectAll(".georgia path")
      .on('mousemove', null)
      .style("cursor","inherit");

    svg.selectAll("g").transition().duration(800)
      .attr("transform", "translate(" + mapw / 3 + "," + h / 2.5 + ")scale(" + 3.33 + ")translate(" + -(x/2.31) + "," + -(y/1.6) + ")")
          .transition().delay(800).duration(1000)
            .style("opacity","0");

    var atlzip = svg.append("g")
        .attr("class", "atl_zips")
      .selectAll("path")
        .data(atl_zips)
      .enter().append("path")
        .attr("transform", "translate(" + mapw / 3 + "," + h / 2.5 + ")scale(" + k + ")translate(" + -x + "," + -y + ")")
        .attr("d", path)
        .style("fill", function(d) { 
          return this_quantize(+d.properties.fema_count)
        });
       
      
      atlzip.style("opacity","0").transition().delay(600).duration(750)
        .style("opacity","1");


  }

}

MapFunctions.drawStates();
$('#text-row h3').empty().html(content[0].header);
$('#text-row p').empty().html(content[0].text);


var func = _.keys(MapFunctions);
  
$('#next').click(function(){
  currentID++;
  if (0 <= currentID < (func.length-1)){ 
    $('#back').show();
    MapFunctions[func[currentID]]();
    $('#text-row h3').empty().html(content[currentID].header);
    $('#text-row p').empty().html(content[currentID].text);
  }
  if (currentID===(func.length-1)){
    $('#next').hide();
  }

})
$('#back').click(function(){
  currentID--;
  if (currentID===0){
    $('#back').hide();
  }
  if (0 <= currentID < func.length){ 
    $('#next').show();
    MapFunctions[func[currentID]]();
    $('#text-row h3').empty().html(content[currentID].header);
    $('#text-row p').empty().html(content[currentID].text);
  }
});